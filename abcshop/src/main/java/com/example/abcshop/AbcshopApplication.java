package com.example.abcshop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AbcshopApplication {

    public static void main(String[] args) {
        SpringApplication.run(AbcshopApplication.class, args);
    }

}
