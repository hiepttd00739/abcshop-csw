package com.example.abcshop.controller;

import com.example.abcshop.entity.Product;
import com.example.abcshop.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1")
public class ProductController {

    @Autowired
    private ProductRepository productRepository;

    public ProductRepository getProductRepository() {
        return productRepository;
    }

    public void setProductRepository(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @GetMapping(value = "/products")
    public List<Product> getAllProducts(){
        return (List<Product>) productRepository.findAll();
    }

    @PostMapping("/products")
    public Product addProduct(@RequestBody Product newProduct) {
        return productRepository.save(newProduct);
    }

    @GetMapping("products/sell/{productId}/{quantity}")
    Product sellProduct(@PathVariable int productId,@PathVariable int quantity) {
        Product productSell = productRepository.findById(productId).get();
        productSell.setQuantity(productSell.getQuantity() - quantity);
        return productRepository.save(productSell);
    }
}
